﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundRepeater : MonoBehaviour
{
    private float halfHeight;
    private SpriteRenderer spriteRenderer;
    public GameObject background;
    public GameObject[] layouts;
    private bool created = false;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        halfHeight = spriteRenderer.bounds.size.y / 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (!created && Camera.main.transform.position.y + GameManager.Instance.CameraBounds.y > transform.position.y + halfHeight)
        {
            GameObject newBG = Instantiate(background, transform.position, Quaternion.identity);
            GameObject newLayout = Instantiate(layouts[(int)Random.Range(0, layouts.Length-1)], transform.position, Quaternion.identity);
            newBG.transform.position += new Vector3(0, halfHeight * 2, 0);
            newLayout.transform.position = newBG.transform.position;
            created = true;
        }    
    }
}
