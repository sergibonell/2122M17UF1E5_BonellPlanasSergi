﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetection : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("HP"))
        {
            if(GameManager.Instance.Health < 3)
                GameManager.Instance.AddToHP(1);
            Destroy(collision.gameObject);
        }else if (collision.gameObject.CompareTag("Weapon"))
        {
            GameManager.Instance.CanShoot = true;
            Destroy(collision.gameObject);
        }
    }
}
