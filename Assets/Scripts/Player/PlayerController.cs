﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject bullet;
    public float moveSpeed;
    public float jumpForce;
    public bool midAir = false;
    private int jumpsAvailable = 2;
    private Animator playerAnimator;
    private SpriteRenderer playerSR;
    private Rigidbody2D playerRB;

    // Start is called before the first frame update
    void Start()
    {
        playerAnimator = GameManager.Instance.Player.GetComponent<Animator>();
        playerSR = GameManager.Instance.Player.GetComponent<SpriteRenderer>();
        playerRB = GameManager.Instance.Player.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float horzAxis = Input.GetAxis("Horizontal");

        // Horizontal movement
        if ((horzAxis > 0 && transform.position.x < GameManager.Instance.CameraBounds.x) || (horzAxis < 0 && transform.position.x > -GameManager.Instance.CameraBounds.x))
        {
            playerAnimator.SetBool("Moving", true);
            playerAnimator.SetFloat("Speed", Mathf.Abs(horzAxis));
            playerSR.flipX = horzAxis < 0;
            transform.Translate(Vector3.right * horzAxis * Time.deltaTime * moveSpeed);
        }
        else
            playerAnimator.SetBool("Moving", false);  

        // Jumping
        if (Input.GetKeyDown(KeyCode.Space) && jumpsAvailable > 0)
        {
            playerAnimator.SetBool("Jump", true);
            playerRB.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            jumpsAvailable--;
        }

        // Shooting
        if (GameManager.Instance.CanShoot && Input.GetKeyDown(KeyCode.X))
            Instantiate(bullet, new Vector3(transform.position.x, transform.position.y + .2f, transform.position.z), Quaternion.identity);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            Vector2 normal = collision.contacts[0].normal;
            if(normal.y > 0)
            {
                GameManager.Instance.CurrentPlatform = collision.gameObject;
                playerAnimator.SetBool("Jump", false);
                playerAnimator.SetBool("Mid Air", false);
                jumpsAvailable = 2;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            GameManager.Instance.CurrentPlatform = null;
            playerAnimator.SetBool("Mid Air", true);
        }
    }
}
