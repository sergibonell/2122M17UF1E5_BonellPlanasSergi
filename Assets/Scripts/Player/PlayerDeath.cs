﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{ 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            if (GameManager.Instance.AddToHP(-1) != 0)
                GameManager.Instance.GetHit();
            else
                GameManager.Instance.GameOver();
        }
    }
}
