﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    private AudioSource squish;

    // Start is called before the first frame update
    void Start()
    {
        squish = GetComponent<AudioSource>();
    }

    public void playSquish()
    {
        squish.Play();
    }
}
