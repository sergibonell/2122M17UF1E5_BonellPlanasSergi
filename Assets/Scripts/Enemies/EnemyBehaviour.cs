﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Kinda better solution :)

public class EnemyBehaviour : MonoBehaviour
{
    public float enemySpeed = 1f;
    private GameObject platform;
    private Transform player;
    private Vector3 enemyLeft;
    private Vector3 enemyRight;

    // Start is called before the first frame update
    void Start()
    {
        enemyLeft = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        enemyRight = new Vector3(enemyLeft.x * -1, enemyLeft.y, enemyLeft.z);
        player = GameManager.Instance.Player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (platform == GameManager.Instance.CurrentPlatform)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, enemySpeed * Time.deltaTime);
            if (transform.position.x < player.position.x)
                transform.localScale = enemyRight;
            else
                transform.localScale = enemyLeft;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
            platform = collision.gameObject;
    }
}
