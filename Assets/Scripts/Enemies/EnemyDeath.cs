﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            // 50% chance to drop an HP object
            if (Random.value > 0.5f)
                Instantiate(GameManager.Instance.Life, new Vector3(transform.position.x, transform.position.y + .2f, transform.position.z), Quaternion.identity);

            Destroy(gameObject);
            Destroy(collision.gameObject);
            GameManager.Instance.AddToEnemies(1);
        }
    }
}
