﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteUnderCamera : MonoBehaviour
{
    private float halfHeight;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        halfHeight = spriteRenderer.bounds.size.y / 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (Camera.main.transform.position.y - GameManager.Instance.CameraBounds.y > transform.position.y + halfHeight)
            Destroy(gameObject);
    }
}
