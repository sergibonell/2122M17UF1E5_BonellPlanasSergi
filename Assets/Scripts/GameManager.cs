﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    private PanelLogic panelLogic;
    private IngameUILogic ingameUILogic;
    private Vector3 initialCamera;

    private float currentY;
    private Vector2 _cameraBounds;
    private int _health = 2;
    private float _maxHeight = 0;
    private int _enemies = 0;
    private bool _isOver = false;

    public GameObject Player;
    public GameObject Life;
    public GameObject CurrentPlatform;
    public Vector2 CameraBounds { get { return _cameraBounds; } }
    public int Health { get { return _health; } }
    public float MaxHeight { get { return _maxHeight; } }
    public int Enemies { get { return _enemies; } }
    public bool IsOver { get { return _isOver; } }
    public bool CanShoot { get; set; }

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        CanShoot = false;
        initialCamera = Camera.main.transform.position;
        UpdateCameraBounds();
        panelLogic = HUDManager.Instance.GameOverPanel.GetComponent<PanelLogic>();
        ingameUILogic = HUDManager.Instance.IngameUI.GetComponent<IngameUILogic>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!IsOver)
            currentY = Player.transform.position.y;

        // Check if player falls below camera
        if (currentY < Camera.main.transform.position.y - _cameraBounds.y)
            GameOver();

        if (currentY > _maxHeight)
            ChangeMaxHeight(currentY);
    }

    public void UpdateCameraBounds()
    {
        _cameraBounds = new Vector2(Camera.main.orthographicSize * Screen.width / Screen.height, Camera.main.orthographicSize);
    }

    public int AddToHP(int n)
    {
        _health += n;
        ingameUILogic.UpdateHP();
        return _health;
    }

    public void ChangeMaxHeight(float height)
    {
        _maxHeight = height;
        ingameUILogic.UpdateHeight();
    }

    public void AddToEnemies(int n)
    {
        _enemies += n;
    }

    public void GetHit()
    {
        Camera.main.transform.position = initialCamera;
        UpdateCameraBounds();
        Player.transform.position = Vector3.zero;
    }

    public void GameOver()
    {
        if (PlayerPrefs.HasKey("MaxScore"))
        {
            if (_maxHeight > PlayerPrefs.GetFloat("MaxScore"))
                PlayerPrefs.SetFloat("MaxScore", _maxHeight);
        }
        else
            PlayerPrefs.SetFloat("MaxScore", _maxHeight);

        Destroy(Player);
        HUDManager.Instance.GameOverPanel.SetActive(true);
        HUDManager.Instance.IngameUI.SetActive(false);
        panelLogic.UpdateValues();
        _isOver = true;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
