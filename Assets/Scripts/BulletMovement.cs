﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed;
    private int direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.Instance.Player.GetComponent<SpriteRenderer>().flipX)
            direction = -1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * direction * speed * Time.deltaTime);
        if (transform.position.x > GameManager.Instance.CameraBounds.x || transform.position.x < -GameManager.Instance.CameraBounds.x)
            Destroy(gameObject);
    }
}
