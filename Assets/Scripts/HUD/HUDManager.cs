﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDManager : MonoBehaviour
{
    private static HUDManager _instance = null;

    public static HUDManager Instance
    {
        get { return _instance; }
    }
    public GameObject GameOverPanel;
    public GameObject IngameUI;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }
}
