﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PanelLogic : MonoBehaviour
{
    private Text height;
    private Text max;
    private Text enemies;
    private Button restart;

    // Start is called before the first frame update
    void Awake()
    {
        height = transform.Find("HeightText").gameObject.GetComponent<Text>();
        max = transform.Find("MaxHeightText").gameObject.GetComponent<Text>();
        enemies = transform.Find("EnemiesText").gameObject.GetComponent<Text>();
        restart = transform.Find("RestartButton").gameObject.GetComponent<Button>();

        restart.onClick.AddListener(GameManager.Instance.ReloadScene);
    }

    public void UpdateValues()
    {
        height.text = "Height: " + GameManager.Instance.MaxHeight.ToString("n1");
        max.text = "Max: " + PlayerPrefs.GetFloat("MaxScore").ToString("n1");
        enemies.text = "Enemies: " + GameManager.Instance.Enemies;
    }
}
