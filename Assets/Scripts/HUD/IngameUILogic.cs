﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameUILogic : MonoBehaviour
{
    private Text hpText;
    private Text heightText;

    // Start is called before the first frame update
    void Start()
    {
        hpText = transform.Find("HP").gameObject.GetComponent<Text>();
        heightText = transform.Find("Height").gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateHP()
    {
        hpText.text = "HP: " + GameManager.Instance.Health;
    }

    public void UpdateHeight()
    {
        heightText.text = "Height: " + GameManager.Instance.MaxHeight.ToString("n1");
    }

}
